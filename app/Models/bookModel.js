const db = require('../../config/db.config');
const { ObjectId } = require('mongodb');
const bookCollection = db.collection('books');

/**
 * @desc Get All Books promise
 * @returns false|array
 */
function findAll() {
    return new Promise((resolve, reject) => {
        resolve(bookCollection.find({}).project({ pages: 0 }).toArray());
    });
}

/**
 * @desc Get Single Book promise
 * @param {*} id 
 * @returns false|object
 */
function findById(id) {
    return new Promise((resolve, reject) => {
        if (ObjectId.isValid(id)) {
            resolve(bookCollection.findOne({ _id: ObjectId(id) }));
        }
        resolve(false);
    })
}

/**
 * @desc Get Single Book Page promise
 * @param {*} bookId 
 * @param {*} pageId 
 * @returns false|object
 */
function findPageById(bookId, pageId) {
    return new Promise((resolve, reject) => {
        let book = bookCollection.findOne({ _id: ObjectId(bookId), "pages.id": pageId }, { projection: { pages: 1 } });
        resolve(book);
    })
}


module.exports = {
    findAll,
    findById,
    findPageById
}
