const Book = require('../Models/bookModel')

/**
 * @desc    Get All Books
 * @route   GET /api/books
 * @param {*} req 
 * @param {*} res 
 */
async function getBooks(req, res) {
    try {
        const books = await Book.findAll()
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(books))
    } catch (error) {
        console.log(error)
    }
}

/**
 * @desc    Get Single Book
 * @route   GET /api/books/:id
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} id 
 */
async function getBook(req, res, id) {
    try {
        const book = await Book.findById(id)
        if (!book) {
            res.writeHead(404, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify({ message: 'book not Found' }))
        } else {
            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify(book))
        }
    } catch (error) {
        console.log(error)
    }
}

/**
 * @desc    Get Single Book Page
 * @route   GET /api/books/:id/page/:pageId
 * @param {*} req 
 * @param {*} res 
 * @param {*} bookId 
 * @param {*} pageId 
 * @param {*} type 
 */
async function getBookPage(req, res, bookId, pageId, type) {
    try {
        const book = await Book.findPageById(bookId, pageId)
        if (!book) {
            res.writeHead(404, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify({ message: 'Book page not found.' }))
        } else {
            const page = book.pages.find((p) => p.id === pageId)
            if (type == 'text') {
                page.content = removeTags(page.content);
            }
            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify(page))
        }
    } catch (error) {
        console.log(error)
    }
}


/**
 * @desc Remove all html tags from string
 * @param {*} str 
 * @returns false|string
 */
function removeTags(str) {
    if ((str === null) || (str === ''))
        return false;
    else
        str = str.toString();
    return str.replace(/(<([^>]+)>)/ig, '');
}


module.exports = {
    getBooks,
    getBook,
    getBookPage
}