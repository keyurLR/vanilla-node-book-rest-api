# Library API

A Library API project created for gbh

---
## Requirements

For development, you will only need Node.js and a node global package, installed in your environement.

## Install

    $ https://gitlab.com/keyurLR/vanilla-node-book-rest-api.git
    $ cd vanilla-node-book-rest-api
    $ npm install

## Import collection in MongoDB

    $ mongoimport --db <DB_NAME> --collection books --jsonArray --file collection/books.json

## Configure app

Open `sample.db.config.js` file and create a copy of it.
Rename it to `db.config.js`  then edit it with your settings. You will need:

- MongoDB URI; mongodb://127.0.0.1/<DB_NAME>

## Running the project

    $ npm start

The project will be running on PORT 5000, so it is accessible on http://localhost:5000/api/