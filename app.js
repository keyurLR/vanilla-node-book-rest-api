const http = require('http')
const { getBooks, getBook, getBookPage } = require('./app/Controllers/bookController')

const server = http.createServer((req, res) => {
    // api routes
    if (req.url === '/api/books' && req.method === 'GET') {
        getBooks(req, res)
    } 
    else if (req.url.match(/\/api\/books\/\w+\/page\/+/) && req.method === 'GET') {
        const id = req.url.split('/')[3]
        const pageId = req.url.split('/')[5]
        const type = req.url.split('/')[6]
        getBookPage(req, res, id, pageId, type)
    }
    else if (req.url.match(/\/api\/books\/\w+/) && req.method === 'GET') {
        const id = req.url.split('/')[3]
        getBook(req, res, id, )
    }
    else {
        res.writeHead(404, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify({ message: 'Route Not Found' }))
    }
})

const PORT = process.env.PORT || 5000

server.listen(PORT, () => console.log(`Server running on port ${PORT}`))

module.exports = server;